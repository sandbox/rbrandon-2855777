<?php

namespace Drupal\social_auth_twitter\Settings;

/**
 * Defines an interface for Social Auth Google settings.
 */
interface TwitterAuthSettingsInterface {

  /**
   * Gets the client ID.
   *
   * @return string
   *   The client ID.
   */
  public function getConsumerKey();

  /**
   * Gets the client secret.
   *
   * @return string
   *   The client secret.
   */
  public function getConsumerSecret();

}
