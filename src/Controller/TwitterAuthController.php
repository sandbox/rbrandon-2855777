<?php

namespace Drupal\social_auth_twitter\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\social_auth_twitter\TwitterAuthManager;
use Drupal\social_api\Plugin\NetworkManager;
use Drupal\social_auth\SocialAuthUserManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Zend\Diactoros\Response\RedirectResponse;

/**
 * Manages requests to Twitter API.
 */
class TwitterAuthController extends ControllerBase {

  /**
   * The network plugin manager.
   *
   * @var \Drupal\social_api\Plugin\NetworkManager
   */
  private $networkManager;

  /**
   * The Twitter authentication manager.
   *
   * @var \Drupal\social_auth_twitter\TwitterAuthManager
   */
  private $twitterManager;

  /**
   * The user manager.
   *
   * @var \Drupal\social_auth\SocialAuthUserManager
   */
  private $userManager;

  /**
   * TwitterLoginController constructor.
   *
   * @param \Drupal\social_api\Plugin\NetworkManager $network_manager
   *   Used to get an instance of social_auth_twitter network plugin.
   * @param \Drupal\social_auth_twitter\TwitterAuthManager $twitter_manager
   *   Used to manage authentication methods.
   * @param \Drupal\social_auth\SocialAuthUserManager $user_manager
   *   Manages user login/registration.
   */
  public function __construct(NetworkManager $network_manager, TwitterAuthManager $twitter_manager, SocialAuthUserManager $user_manager) {
    $this->networkManager = $network_manager;
    $this->twitterManager = $twitter_manager;
    $this->userManager = $user_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.network.manager'),
      $container->get('twitter_auth.manager'),
      $container->get('social_auth.user_manager')
    );
  }

  /**
   * Redirect to Twitter Services Authentication page.
   *
   * @return \Zend\Diactoros\Response\RedirectResponse
   *   Redirection to Twitter Accounts.
   */
  public function redirectToTwitter() {
    /* @var \Twitter_Client $client */
    $connection = $this->networkManager->createInstance('social_auth_twitter')->getSdk();
    $token = $this->networkManager->createInstance('social_auth_twitter')->getToken();

    $_SESSION['oauth_token'] = $token['oauth_token'];
    $_SESSION['oauth_token_secret'] = $token['oauth_token_secret'];

    return new RedirectResponse($connection->url('oauth/authorize', array('oauth_token' => $token['oauth_token'])));
  }

  /**
   * Callback function to login user.
   */
  public function callback() {
    /* @var \Twitter_Client $client */
    $client = $this->networkManager->createInstance('social_auth_twitter')->getSdk();
    if ($_SESSION['oauth_token']) {
      $client->setOauthToken($_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);
    }

    $this->twitterManager->setClient($client)->authenticate();

    // If user information could be retrieved.
    if (!empty($this->twitterManager->getAccessToken())) {

      $user_data = $this->twitterManager->getUserInfo();
      if ($user_data->email) {

        // Tries to load the user by his email.
        $drupal_user = $this->userManager->loadUserByProperty('mail', $user_data->email);
        // If user email has already an account in the site.
        if ($drupal_user) {
          if ($this->userManager->loginUser($drupal_user)) {
            return $this->redirect('user.page');
          }
        }

        $drupal_user = $this->userManager->createUser($user_data->screen_name, $user_data->email);
        // If the new user could be registered.
        if ($drupal_user) {
          // If the new user could be logged in.
          if ($this->userManager->loginUser($drupal_user)) {
            return $this->redirect('user.page');
          }
        }
      } else {
        // Possible that the twitter app does not have email access.
        drupal_set_message($this->t('Email address could not be found, your email may not be verified with twitter.'), 'error');
      }
    } else {
      drupal_set_message($this->t('You could not be authenticated, please contact the administrator'), 'error');
    }

    return $this->redirect('user.login');
  }

}
