<?php

namespace Drupal\social_auth_twitter;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Manages the authentication requests.
 */
class TwitterAuthManager {
  /**
   * The session object.
   *
   * @var \Symfony\Component\HttpFoundation\Session\Session
   */
  private $session;

  /**
   * The request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  private $request;

  /**
   * The Twitter client.
   *
   * @var \Twitter_Client
   */
  private $client;

  /**
   * Code returned by Twitter for authentication.
   *
   * @var string
   */
  private $code;

  /**
   * The Twitter Oauth2 object.
   *
   * @var \Twitter_Service_Oauth2
   */
  private $twitterService;

  /**
   * TwitterLoginManager constructor.
   *
   * @param \Symfony\Component\HttpFoundation\Session\Session $session
   *   Used to access and store session variables.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request
   *   Used to get the parameter code returned by Twitter.
   */
  public function __construct(Session $session, RequestStack $request) {
    $this->session = $session;
    $this->request = $request->getCurrentRequest();
  }

  /**
   * Gets the access token.
   *
   * @return array
   *   Array with the token data.
   */
  public function getAccessToken() {
    return $this->session->get('social_auth_twitter_token');
  }

  /**
   * Sets the client object.
   *
   * @param \Abraham\TwitterOAuth\TwitterOAuth $client
   *   Twitter Client object.
   *
   * @return $this
   *   The current object.
   */
  public function setClient(\Abraham\TwitterOAuth\TwitterOAuth $client) {
    $this->client = $client;
    return $this;
  }

  /**
   * Gets the client object.
   *
   * @return \Twitter_Client.
   *   The Twitter Client object.
   */
  public function getClient() {
    return $this->client;
  }

  /**
   * Authenticates the users by using the returned code.
   *
   * @return $this
   *   The current object.
   */
  public function authenticate() {
    $access_token = $this->client->oauth("oauth/access_token", ["oauth_verifier" => $_REQUEST['oauth_verifier']]);

    $this->saveAccessToken('social_auth_twitter_token', $access_token);

    return $this;
  }

  /**
   * Saves the access token.
   *
   * @param string $key
   *   The session key.
   *
   * @return $this
   *   The current object.
   */
  public function saveAccessToken($key, $token) {
    $this->session->set($key, $token);
    return $this;
  }

  /**
   * Returns the user information.
   *
   * @return \Twitter_Service_Oauth2_Userinfoplus.
   *   Twitter_Service_Userinfoplus object.
   */
  public function getUserInfo() {

    // Set the new access token.
    $access_token = $this->getAccessToken();
    $client = $this->getClient();

    $client->setOauthToken($access_token['oauth_token'], $access_token['oauth_token_secret']);

    // Get account and email data.
    return $client->get("account/verify_credentials", ["include_email" => 'true', "include_entities" => 'false', 'skip_status' => 'true']);
  }

  /**
   * Gets the code returned by Twitter to authenticate.
   *
   * @return string
   *   The code string returned by Twitter.
   */
  protected function getCode() {
    if (!$this->code) {
      $this->code = $this->request->query->get('code');
    }

    return $this->code;
  }

}
